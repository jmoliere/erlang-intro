-module(message3).
-export([start/0]).

start() ->
	receive 
		{_,42} -> io:format("Good answer guy~n"),
		start();
		{_,"Hello"} -> io:format("Good day too~n"),
			       start();
          _ -> io:format("Don't know what to do!!!~n"),
	       start()
	end.

