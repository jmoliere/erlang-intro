-module(factorial2).
-export([factorial_acc/1,factorial_acc/2]).

factorial_acc(0,ACC) -> ACC;
factorial_acc(X,ACC) when X>0 -> factorial_acc(X-1,X*ACC).

factorial_acc(N) -> factorial_acc(N,1). 
