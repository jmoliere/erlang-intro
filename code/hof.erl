-module(hof).
-export([mul2/1,testhof/2,show/0]).

mul2(N) -> 2*N.
testhof(Fun,N) -> Fun(N).

show() -> testhof(fun mul2/1,5).


