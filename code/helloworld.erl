% hello world program
% code defined in a module called helloworld
-module(helloworld).

% this module exports a single function (publicly)
% it is start requiring no parameters
-export([start/0]).

% the start function
% just displays a message on console
start() ->
    io:fwrite("Hello, world!\n").
