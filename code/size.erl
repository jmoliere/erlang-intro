-module(size).
-export([mysize/1,printsize/1]).
printsize(List) ->
  Res = size:mysize(List),
  io:fwrite("~w ~w~n",[List,Res]).
mysize([]) -> 0;
mysize([_]) -> 1;
mysize([X|L]) -> mysize(L)+1.

